<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<!DOCTYPE HTML>
<html amp>
<head>
<meta charset="utf-8">
<link rel="canonical" href="https://www.shriramproperties.com/magizhchi-new/thanks.php" />
<title>Thank you | New Launch | Shriram Properties</title>
<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-font" src="https://cdn.ampproject.org/v0/amp-font-0.1.js"></script>
<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<style amp-custom>
*{box-sizing: border-box;}
@font-face{font-family: 'LatoRegular';src: url('fonts/Lato-Regular.eot');src: url('fonts/Lato-Regular.eot?#iefix') format('embedded-opentype'),
        url('fonts/Lato-Regular.woff2') format('woff2'),url('fonts/Lato-Regular.woff') format('woff'),url('fonts/Lato-Regular.ttf') format('truetype');
    font-weight: normal;font-style: normal;}
@font-face{font-family: 'LatoHeavy';src: url('fonts/Lato-Heavy.eot');src: url('fonts/Lato-Heavy.eot?#iefix') format('embedded-opentype'),
        url('fonts/Lato-Heavy.woff2') format('woff2'),url('fonts/Lato-Heavy.woff') format('woff'),url('fonts/Lato-Heavy.ttf') format('truetype');
    font-weight: 900;font-style: normal;}
@font-face{font-family: 'LatoSemibold';src: url('fonts/Lato-Semibold.eot');src: url('fonts/Lato-Semibold.eot?#iefix') format('embedded-opentype'),
        url('fonts/Lato-Semibold.woff2') format('woff2'),url('fonts/Lato-Semibold.woff') format('woff'),url('fonts/Lato-Semibold.ttf') format('truetype');
    font-weight: 600;font-style: normal;}
@font-face{font-family: 'LatoBold';src: url('fonts/Lato-Bold.eot');src: url('fonts/Lato-Bold.eot?#iefix') format('embedded-opentype'),
        url('fonts/Lato-Bold.woff2') format('woff2'),url('fonts/Lato-Bold.woff') format('woff'),url('fonts/Lato-Bold.ttf') format('truetype');
    font-weight: bold;font-style: normal;}
body{font-family: 'LatoRegular';font-size:16px;color:#5d5d5d;overflow-x:hidden;	background:#FCFCFC}
a{text-decoration:none;	transition:all 0.5s;color:#fe9200}
a:hover,a:focus,a:active{text-decoration:none;outline:0}
img{max-width:100%}
strong,.lato-bold{font-family: 'LatoBold';font-weight:normal;font-style:normal}
section{position:relative}
.lato-heavy,h1,h2,h3,h4,h5,h6{font-family: 'LatoHeavy';}
.lato-semibold{font-family: 'LatoSemibold';}
.header{position:absolute;top:0;left:0;width:100%;z-index:9;}
.site-logo,.site-logo-holder{line-height:95px}
.banner-caption-form{position:relative;z-index:99;top:0;left:0;width:100%}
.color-white{color:#fff}
.form-holder{background:#ffce2e;z-index:999;padding: 15px 30px;}
.form-holder:after{background:#ffce2e;content:'';height:35px;width:100%;top:100%;position:absolute;left:0;}
.form-holder:before{content:'';height:40px;width:100%;top: calc(100% + 15px);position: absolute;left: 0;background: #FCFCFC;z-index: 9;}
.form-title,.form-title>a{color:#3e3e3e}
.form-control{border-color:#cfcfcf;border-radius:0;color:#727272;width:100%;padding:15px 20px;margin-bottom:20px;}
.form-control:focus{border-color: #fad322;box-shadow: 0 0 0 0.2rem rgba(254,153,0,.25);}
.btn-primary{border-radius:0;border-color:#03405f;background:#03405f;font-family: 'PlayFairDisplayRegular';font-size:18px;padding:8px 30px}
.btn-primary.disabled, .btn-primary:disabled{background-color: #03405f;border-color: #03405f}
.btn-primary:hover{color: #03405f;background-color: #fff;border-color: #03405f}
.help-block.with-errors{color:#a00;font-size:12px}
.sticky-btns{font-size:0;position:fixed;bottom:0;left:0;width:100%;z-index:999}
.sticky-btns a{background:#ffce2e;color:#000;border-top:1px solid #fff;border-right:1px solid #fff;width:100%;text-align:center;display:inline-block;	padding:8px 0;font-size:18px;}
.sticky-btns a:last-child{border-right:none;}
.dummy-div{height:43px;}
.text-center{text-align:center} .pt-10{padding-top:10px;} .pb-10{padding-bottom:10px;} .fs-18{font-size:18px;}
</style>
<body>
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-WBDZLSJ&gtm.url=https://www.shriramproperties.com/magizhchi-new/thanks.php" data-credentials="include"></amp-analytics>
<section class="banner">	
	<amp-img src="images/banner-mob.png" class="d-block d-sm-block d-md-none d-lg-none w-100" alt="" width="480" height="768" layout="responsive" /></amp-img>
	<div class="banner-caption-form">
		<div class="form-banner-caption-holder">				
			<div class="col-12 col-sm-12 offset-md-9 col-md-3 form-holder">
				<h5 class="mt-0 fs-18 form-title playfair-bold text-center pt-10 pb-10 text-uppercase">Thank you for your enquiry</h5>
				<h6 class="mt-0 fs-18 form-title playfair-bold text-center pt-0 pb-0 text-uppercase">Our team will get back to you shortly</h6>
			</div>
		</div>
	</div>
</section>