<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
<!--<link rel="amphtml" href="http://www.bestechgrandspa.in/amp" />-->
<title>Thank You | New Launch | Shriram Properties</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/swiper.min.css" rel="stylesheet" type="text/css" />
<link href="css/padd-mar.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5648C5B');</script>
</head>
<body class="home">
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5648C5B" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-sm-6 col-md-3 text-left d-block d-md-block d-lg-block d-sm-block site-logo-holder mobile-image-size">
				<img src="images/project-logo.png" alt="" class="site-logo" />
			</div>
			<div class="col-6 col-md-6 col-sm-12 text-center mt-0 padding-0 d-none d-sm-none d-md-block d-lg-block d-xl-block">
				<div id="pageTimer" class="bg-yellow seven">
					<div class=""></div>
				</div>
			</div>
			<div class="col-6 col-sm-6 col-md-3 text-right site-logo-holder mobile-image-size">
				<img src="images/logo.png" alt="" class="site-logo" />
			</div>
			<div class="col-12 text-center mt-0 padding-0 d-block d-sm-block d-md-none d-lg-none d-xl-none" style="padding-0">
				<div id="pageTimermob" class="bg-yellow seven">
					<div class=""></div>
				</div>
			</div>
		</div>
	</div>
</header>
<section class="banner pt-xs-0">
<img src="images/banner.png" class="d-none d-sm-none d-md-block d-lg-block w-100">
	<img src="images/banner_mob.png" class="d-block d-sm-block d-md-none d-lg-none w-100">
	<div class="banner-caption-form">
		<div class="container">
			<div class="row form-banner-caption-holder">				
				<div class="col-12 col-sm-12 offset-md-9 col-md-3 form-holder" id="form-holder">
					<h5 class="mt-35 mb-0 fs-24 form-title playfair-bold text-center pt-10 text-uppercase">Thank You</h5>
					<h6 class="mt-15 mb-35 fs-20 form-title playfair-bold text-center pb-10 text-uppercase">We will get in touch with you shortly.</h6>
				</div>
			</div>
		</div>
	</div>
</section>
<section style="background-color:#414141;color:#fff;">
	<div class="container">
		<div class="row">
			<div class="col-12 pt-10 text-center">
				<p class="gill-medium fs-20 fs-xs-16 fs-sm-16">&copy; Copyright 2018, All rights reserved with Shriram Properties Private Limited.</p>
			</div>
		</div>
	</div>
</section>
<div class="dummy-div d-block d-sm-block d-md-none d-lg-none"></div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<link href="css/combined.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" defer></script>
<script type="text/javascript" src="js/bootstrap.min.js" defer></script>
<script type="text/javascript" src="js/swiper.min.js" defer></script>
<script type="text/javascript" src="js/validator.min.js" defer></script>
<script type="text/javascript" src="js/custom.js" defer></script>

<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10021212'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&.yp=10021212"/>
<script data-obct type="text/javascript">
  /** DO NOT MODIFY THIS CODE**/
  !function(_window, _document) {
    var OB_ADV_ID='00fad84d0b9cc34b4268a5f99834c729b7';
    if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
    var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
obApi('track', 'PAGE_VIEW');
</script>
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view'});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1095912/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='//trc.taboola.com/1095912/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript>
</body>
</html>